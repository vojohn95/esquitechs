<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Esquitechs -  Desarrollo web, diseñamos paginas web, ecommerce, posicionamiento SEO, WordPress en CDMX y México">
    <meta content="Modernizamos empresas, además de ofrecer un servicio personalizado a nuestros clientes de acuerdo a sus necesidades."/>
    <meta content="Desarrollo de software a la medida" property="og:title">
    <meta content="https://esquitechs.com" property="og:url">
    <meta content="esquitechs" property="og:site_name">
    <meta content="website" property="og:type">
    <meta name="author" content="Esquitechs">
    <meta name="google-site-verification" content="YwjDSVCEZcMXNjIFi3XDiHct3uvIQ6i6VjpN2jMTVGc" />
    <meta name="copyright" content="Esquitechs" />
    <meta property="og:locale" content="es_MX">
    <meta property="og:image" content="https://esquitechs.com/assets/images/Imagen33.webp">
    <meta property="og:description"
          content="Esquitechs - Codificar la realidad no es tan dificil Desarrollo web, diseñamos paginas web, ecommerce, posicionamiento SEO,WordPress en CDMX y México">
    <meta property="og:site_name" content="esquitechs">
    <meta property="article:publisher"
          content="https://es-la.facebook.com/pages/category/Computer-Company/Esquitechs-MX-240796616838427/">
    <meta name="Keywords" content="tutorial, html, sitio web, Desarrollo web, Comercio electrónico, Diseño web, Website builder, Diseño, Consultoria , php, javascript, css, bootstrap, material design, google, operadora central de estacionamientos, sarivia talent, unitec, cdmx, desarrollo web cdmx, México,agencia de diseño web,campañas de facebook,campañas de marketing,campañas en facebook,desarrollo de aplicaciones web,desarrollo de apps,desarrollo de paginas web,desarrollo de software,desarrollo paginas web,desarrollo software,diseño web,diseño y desarrollo web,diseños modernos,elaboracion de paginas web,flutter,marketing,marketing digital,marketing online,material design,material design web,mercadotecnia online,posicionamiento de paginas web,posicionamiento en buscadores,posicionamiento seo,posicionamiento web,posicionamiento web seo,posicionar pagina web,programacion web,programador,programador php,programador web,seo,programadores,it services, system integrators, erp software, outsourcing strategy, strategic sourcing, offshore software development, outsourcing companies, global sourcing, application outsourcing services, enterprise application portfolio management,integradores de tecnologia,integradores tecnologia,grupo empresarial,soluciones TI,alianzas estrategicas,empresa mexicana,desarrollo de lideres,mejoramiento economico,desarrollo de empresarios, DISEÑO WEB, DISEÑO DE PAGINAS WEB, ELABORACION DE PAGINAS WEB, DESARROLLO WEB, CREACION DE PAGINAS WEB, PAGINAS WEB ECONOMICAS, PAGINAS EN INTERNET, REGISTRO DE DOMINIOS, CORREO ELECTRONICO, SEO, POSICIONAMIENTO WEB, PAGINA DE INTERNET, PAGINAS PARA EMPRESAS, PROMOCION EN PAGINAS WEB, GOOGLE ADWORDS, CAMPAÑAS EN GOOGLE, PAGINAS WEB PROFESIONALES, CUANTO CUESTA UNA PAGINA WEB, PAGINAS WEB PRECIOS, DISEÑO DE PAGINAS WEB EN CDMX, DISEÑO DE PAGINAS WEB EN EL DF"/>
    <meta name="robots" content="index,follow"/>
    <meta http-equiv="expires" content="3600"/>
    <link rel="icon" type="icon" href="assets/images/Imagen66.webp"/>
    <meta name="revisit-after" content="14 days">
    <meta name="distribution" content="global">
    <!-- SITE TITLE -->
    <title>Desarrollo de páginas web y aplicaciones | Esquitechs</title>

    <link rel=”alternate” hreflang=”es-MX” href=”https://esquitechs.com/”/>
    <link rel=”canonical” href=”https://esquitechs.com/"/>
    <!-- Google Tag Manager -->
    <!--Se realiza la carga de search console-->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NLMSXSH');</script>
    <!-- End Google Tag Manager -->

</head>
<!--<style>
    :root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;-ms-overflow-style:scrollbar}@-ms-viewport{width:device-width}header,nav,section{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}hr{box-sizing:content-box;height:0;overflow:visible}h1,h4,h5{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}ul{margin-top:0;margin-bottom:1rem}a{color:#007bff;text-decoration:none;background-color:transparent;-webkit-text-decoration-skip:objects}img{vertical-align:middle;border-style:none}svg:not(:root){overflow:hidden}button{border-radius:0}button{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}button{overflow:visible}button{text-transform:none}button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,button::-moz-focus-inner{padding:0;border-style:none}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}h1,h4,h5{margin-bottom:.5rem;font-family:inherit;font-weight:500;line-height:1.2;color:inherit}h1{font-size:2.5rem}h4{font-size:1.5rem}h5{font-size:1.25rem}hr{margin-top:1rem;margin-bottom:1rem;border:0;border-top:1px solid rgba(0,0,0,.1)}.container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}.container-fluid{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-lg-6,.col-md-12,.col-md-4,.col-md-8,.col-sm-12{position:relative;width:100%;min-height:1px;padding-right:15px;padding-left:15px}.order-first{-webkit-box-ordinal-group:0;-ms-flex-order:-1;order:-1}@media (min-width:576px){.col-sm-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:768px){.col-md-4{-webkit-box-flex:0;-ms-flex:0 0 33.333333%;flex:0 0 33.333333%;max-width:33.333333%}.col-md-8{-webkit-box-flex:0;-ms-flex:0 0 66.666667%;flex:0 0 66.666667%;max-width:66.666667%}.col-md-12{-webkit-box-flex:0;-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}@media (min-width:992px){.col-lg-6{-webkit-box-flex:0;-ms-flex:0 0 50%;flex:0 0 50%;max-width:50%}.order-lg-first{-webkit-box-ordinal-group:0;-ms-flex-order:-1;order:-1}}.btn{display:inline-block;font-weight:400;text-align:center;white-space:nowrap;vertical-align:middle;border:1px solid transparent;padding:.375rem .75rem;font-size:1rem;line-height:1.5;border-radius:.25rem}.btn-info{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.collapse{display:none}.nav-link{display:block;padding:.5rem 1rem}.navbar{position:relative;display:-webkit-box;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between;padding:.5rem 1rem}.navbar-brand{display:inline-block;padding-top:.3125rem;padding-bottom:.3125rem;margin-right:1rem;font-size:1.25rem;line-height:inherit;white-space:nowrap}.navbar-nav{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;padding-left:0;margin-bottom:0;list-style:none}.navbar-nav .nav-link{padding-right:0;padding-left:0}.navbar-collapse{-ms-flex-preferred-size:100%;flex-basis:100%;-webkit-box-flex:1;-ms-flex-positive:1;flex-grow:1;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.navbar-toggler{padding:.25rem .75rem;font-size:1.25rem;line-height:1;background-color:transparent;border:1px solid transparent;border-radius:.25rem}@media (min-width:992px){.navbar-expand-lg{-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-flow:row nowrap;flex-flow:row nowrap;-webkit-box-pack:start;-ms-flex-pack:start;justify-content:flex-start}.navbar-expand-lg .navbar-nav{-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-direction:row;flex-direction:row}.navbar-expand-lg .navbar-nav .nav-link{padding-right:.5rem;padding-left:.5rem}.navbar-expand-lg .navbar-collapse{display:-webkit-box!important;display:-ms-flexbox!important;display:flex!important;-ms-flex-preferred-size:auto;flex-basis:auto}.navbar-expand-lg .navbar-toggler{display:none}}.align-items-center{-webkit-box-align:center!important;-ms-flex-align:center!important;align-items:center!important}.fixed-top{position:fixed;top:0;right:0;left:0;z-index:1030}.w-100{width:100%!important}.m-0{margin:0!important}.mt-3{margin-top:1rem!important}.pt-3{padding-top:1rem!important}.m-auto{margin:auto!important}.text-center{text-align:center!important}
</style>-->
<style>
    body{background:#ffffff none repeat scroll 0 0;color:#7a7a7a;font-family:'Poppins',sans-serif;font-size:16px;font-weight:400;line-height:30px}html,body{height:100%}h1,h4,h5{color:#5957cd}p{color:#7a7a7a;margin-bottom:30px}a{color:#5957cd;text-decoration:none}img{max-width:100%}ul,li{margin:0;padding:0}::-webkit-input-placeholder{color:#7a7a7a;font-weight:400;font-family:'Poppins',sans-serif}:-moz-placeholder{color:#7a7a7a;font-weight:400;font-family:'Poppins',sans-serif}::-moz-placeholder{color:#7a7a7a;font-weight:400;font-family:'Poppins',sans-serif}:-ms-input-placeholder{color:#7a7a7a;font-weight:400;font-family:'Poppins',sans-serif}section{padding:100px 0;position:relative}button{background-image:-webkit-linear-gradient(left,#444bff 0%,#170062 99%);background-image:-webkit-linear-gradient(left,#444bff 0%,#170062 99%);border:0;border-radius:40px;color:#ffffff;padding:10px 20px;text-transform:uppercase}.blue_bg{background-color:#193dc0}.container,.container-fluid{position:relative;z-index:5}.btn{border:0 none;border-radius:5px;font-size:16px;font-weight:500;height:54px;line-height:54px;padding:0 25px;position:relative;text-transform:uppercase;vertical-align:middle}.btn.btn-radius{border-radius:40px}.btn i{font-size:30px;line-height:24px;margin-left:5px;padding-top:4px;vertical-align:middle}.btn-info{background:#5957cd none repeat scroll 0 0;height:auto;line-height:normal;padding:12px 30px}.scrollup{border-radius:4px;bottom:30px;color:#ffffff;display:none;font-size:30px;height:40px;line-height:40px;position:fixed;right:20px;text-align:center;width:40px;z-index:99}.btn+.btn:last-child{margin-left:10px}header{height:110px;padding:25px 50px}.navbar{padding:0}.navbar-brand{margin:0;padding:0}header .navbar-nav a{color:#ffffff}.navbar-expand-lg .navbar-nav>li{padding:0 12px}.navbar-expand-lg .navbar-nav>li:last-child{padding-right:0}.navbar-expand-lg .navbar-nav .nav-link{padding:15px 0;font-weight:500;position:relative;text-transform:uppercase}.navbar-expand-lg .nav_btn>li{margin-left:15px;padding:0}.navbar-expand-lg .nav_btn>li:first-child{margin:0}.navbar-expand-lg .navbar-nav li{position:relative}.transparent_effect{height:100%;opacity:0.4;position:absolute;top:0}.section_banner{background-position:left bottom;background-size:cover;padding:200px 0 100px;position:relative;overflow:hidden}.banner_text h1{color:#ffffff;font-weight:bold;margin-bottom:25px}.banner_text p{color:#fff}.banner_image_right{min-width:690px}.btn_group .btn{display:inline-block}.banner_img{text-align:right}.banner_rounded_shape::before{background-color:rgba(8,22,139,0.3);border-radius:100%;content:"";height:968px;left:-160px;opacity:1;position:absolute;right:0;top:-165px;transform:skew(-1deg);width:1240px;z-index:0}.banner_rounded_shape::after{background-color:rgba(8,22,139,0.3);border-radius:100%;content:"";height:968px;left:-200px;opacity:1;position:absolute;right:0;top:-205px;transform:skew(-1deg);width:1240px;z-index:0}.banner_section{padding-bottom:200px;padding-top:120px}.banner_wave{bottom:0;left:0;position:absolute;right:0;fill:#ffffff}.team_title{background-image:-webkit-linear-gradient(to bottom,rgba(0,0,0,0) 0%,#000000 99%);background-image:linear-gradient(to bottom,rgba(0,0,0,0) 0%,#000000 99%);bottom:0;left:0;padding:70px 0 10px;position:absolute;right:0}.team_img_wrap{position:relative}.team_title h4{color:#ffffff!important;margin:0}.team_title span{color:#fff}.team_pop{background-color:#ffffff;border-radius:10px;margin:30px auto;max-width:945px;padding:30px 10px;position:relative}.v_blue_light .team_pop h4,.v_blue_light .team_pop h5{color:#0043bb}.v_blue_light .btn-info{background-color:#193dc0}@keyframes rotate{from{transform:rotate(0deg)}to{transform:rotate(360deg)}}@-webkit-keyframes rotate{from{-webkit-transform:rotate(0deg)}to{-webkit-transform:rotate(360deg)}}.logo_esquitechs{-webkit-animation:5s rotate linear infinite;animation:3s rotate linear infinite;-webkit-transform-origin:50% 50%;transform-origin:50% 50%}
</style>

<body class="v_blue_light" data-spy="scroll" data-offset="110">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLMSXSH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- START LOADER
<div id="loader-wrapper">
    <div id="loading-center-absolute">
        <div class="object" id="object_four"></div>
        <div class="object" id="object_three"></div>
        <div class="object" id="object_two"></div>
        <div class="object" id="object_one"></div>
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>
 END LOADER -->

<!-- START HEADER -->
<header class="header_wrap fixed-top">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg">
            <h1><font color="white" style="font-display;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></h1>
            <a class="navbar-brand page-scroll animation" href="#home_section" data-animation="fadeInDown"
               data-animation-delay="1s">
                <img class="logo_light logo_esquitechs" src="assets/images/Imagen66.webp" id="logo_esquitechs"
                     style="width: 60px;" alt="esquitechs">
            </a>
            <button class="navbar-toggler animation " type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation" data-animation="fadeInDown" data-animation-delay="1.1s">
                <span class="ion-android-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav m-auto">
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.1s"><a
                                class="nav-link page-scroll nav_item" href="#home_section">Inicio</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.3s"><a
                                class="nav-link page-scroll nav_item" href="#whychoose">Nosotros</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.5s"><a
                                class="nav-link page-scroll nav_item" href="#roadmap">Proyectos</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.6s"><a
                                class="nav-link page-scroll nav_item" href="#team">Metodologias</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.4s"><a
                                class="nav-link page-scroll nav_item" href="#token">Servicios</a></li>
                    <!--<li class="animation" data-animation="fadeInDown" data-animation-delay="1.6s"><a class="nav-link page-scroll nav_item" href="#blog">Blog</a></li>-->
                    <!--<li class="animation" data-animation="fadeInDown" data-animation-delay="1.7s"><a class="nav-link page-scroll nav_item" href="#faq">Nuestro equipo</a></li>-->
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.8s"><a
                                class="nav-link page-scroll nav_item" href="#contact">Contacto</a></li>
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="2.1s"><a
                                class="nav-link page-scroll nav_item" href="https://www.facebook.com/esquitechs/"><i class="fa fa-facebook-square" style="font-size:36px;color:white;font-display"></i></a></li>
                </ul>
                <ul class="navbar-nav nav_btn align-items-center">
                    <li class="animation" data-animation="fadeInDown" data-animation-delay="1.9s">
                        <div class="lng_dropdown">

                        </div>
                    </li>
                    <!--<li class="animation" data-animation="fadeInDown" data-animation-delay="2s"><a class="btn btn-default btn-radius nav_item" href="login.html">Login</a></li>-->
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- END HEADER -->

<!-- START SECTION BANNER -->
<section id="home_section" class="section_banner banner_section blue_bg banner_rounded_shape">
    <canvas id="banner_canvas" class="transparent_effect"></canvas>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12 col-sm-12 order-lg-first">
                <div class="banner_text text_md_center res_md_mb_30 res_xs_mb_20">
                    <h1 align="center">
                        ESQUITECHS <img class="logo_light logo_esquitechs " src="assets/images/Imagen66.webp"
                                        id="logo_esquitechs" style="width: 60px;" alt="Esquitechs"></h1>
                    <p>Modernizamos empresas,
                        además de ofrecer un servicio personalizado a nuestros clientes de acuerdo a sus
                        necesidades.</p>
                    <div class="btn_group animation" data-animation="fadeInUp" data-animation-delay="1.4s">
                        <button class="btn btn-info">Desarrollo de software</button>
                        <button class="btn btn-info">Marketing</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 order-first">
                <div class="banner_image_right banner_img animation" data-animation-delay="1.5s"
                     data-animation="fadeInRight">
                    <img  alt="animación web" src="assets/images/banner_img3.webp">
                </div>
            </div>
        </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1"
         x="0px" y="0px" viewbox="0 0 962 96" style="enable-background:new 0 0 962 115;" xml:space="preserve"
         preserveaspectratio="none" class="banner_wave">
		<path class="st0" d="M0,0c0,0,100,94,481,95C862,94,962,0,962,0v115H0V0z"></path>
    </svg>
</section>
<!-- END SECTION BANNER -->

<!-- START SECTION ABOUT US -->
<section id="about" style="background-color: #ebfbfb;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-center res_md_mb_30 res_sm_mb_20">
                    <img class="animation lazyload"" data-animation="zoomIn" data-animation-delay="0.2s"
                         data-src="assets/images/celular.webp" alt="Desarrollo movil">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 text_md_center">
                <div class="title_blue_dark">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s" align="center">
                        MISION</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s" align="justify">
                        Satisfacer la necesidad de la industria de cualquier indole, ofreciendo herramientas y
                        soluciones informatives vanguardistas , posicionandonos como una empresa lider y reconocida por
                        diferentes partes de la republica.</p>
                    <!--<h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s" align="center">VISION</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s" align="justify">Llegar a ser una empresa de prestigio en innovación siendo parte de la era de la información y de las Tecnologias 3.0 y sus subsecuentes.</p>
                        -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION ABOUT US -->

<!-- START SECTION DIGITAL NETWORK -->

<section id="whychoose" class="blue_bg">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="res_md_mb_30 res_sm_mb_20">
                    <div class="title_default_dark title_border">
                        <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s" align="center"><font
                                    color="white">Nosotros</font></h4>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s" align="justify"><font
                                    color="white">Somos una empresa 100% Mexicana, nos enfocamos en las necesidades y
                                requerimientos de nuestros clientes en materia tecnológica, diseñando, desarrollando e
                                implementando software a la medida.
                                Al ser una empresa tan versátil y completa podemos integrar y modificar sus sistemas ya
                                existentes a sus nuevos requerimientos.
                            </font></p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s"><font color="white">¿Estas
                                preparado para el futuro? </font></p>
                        <p class="animation" data-animation="fadeInUp" data-animation-delay="0.8s"><font color="white">La
                                tecnología envoluciona rapidamente, por lo cual estamos preparandonos día con día y
                                adaptadonos a las necesidades del mercado asi como a las nuevas tendencias.</font></p>
                    </div>
                    <a href="404.html" class="btn btn-primary video animation" data-animation="fadeInUp"
                       data-animation-delay="1s"><span class="ion-play"></span><font color="white">Presione aquí</font></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="text-center">
                    <img class="animation lazyload" data-animation="zoomIn" data-animation-delay="0.2s"
                         data-src="assets/images/investment_network.webp" alt="Desarrollo web">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- END SECTION DIGITAL NETWORK -->


<section class="small_pt">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 offset-lg-2 col-md-12 col-sm-12">
                <div class="title_default_dark title_border text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">¿Que hacemos?</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">En Esquitechs ofrecemos
                        modernización, innovación y agilidad de los procesos de las empresas.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box_wrap radius_box bg-white text-center animation" data-animation="fadeInUp"
                     data-animation-delay="0.6s">
                    <img class="lazyload" data-src="assets/images/computer.webp" alt="Desarrollo web y aplicaciones">
                    <h4>Creación de software personalizado</h4>
                    <p>Diseños impresionantes, ponemos el desarrollo en nuestros servidores con opción a moverlo a uno
                        propio, actualización y escabilidad, multiplataforma, auditorias de seguridad.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="box_wrap radius_box bg-white text-center animation" data-animation="fadeInUp"
                     data-animation-delay="0.8s">
                    <img class="lazyload" data-src="assets/images/data.webp" alt="Hosting y alojamiento">
                    <h4>Servicio de hosting</h4>
                    <p>Esquitechs tiene una amplia variedad de servidores, no importa el tamaño de tu compañía o negocio
                        e tenemos una solución, ofrecemos una Alta disponibilidad, seguridad y sistemas operativos.</p>
                </div>
            </div>
            <div class="col-lg-4 offset-lg-0 col-md-6 offset-md-3 col-sm-12">
                <div class="box_wrap radius_box bg-white text-center animation" data-animation="fadeInUp"
                     data-animation-delay="1s">
                    <img class="lazyload" data-src="assets/images/seguridad.webp" alt="software erp">
                    <h4>Software ERP</h4>
                    <p>¿Quieres optimizar tus procesos de negocio? ofrecemos software modular en la nube, el cual
                        proporciona integridad y unicidad de la información, ahorro de tiempo y costos, aumento de la
                        productividad y seguridad.</p>
                </div>
            </div>
        </div>
    </div>
</section>


<!---->
<!-- START SECTION COUNTER -->
<section class="counter_wrap overlay background_bg counter_bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center res_sm_mb_20">
                    <i class="ion-ios-pie animation" data-animation="fadeInUp" data-animation-delay="0.2s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="0.3s">35</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s">Sitios alojados</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center res_sm_mb_20">
                    <i class="ion-help-buoy animation" data-animation="fadeInUp" data-animation-delay="0.5s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="0.6s">20</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.7s">Soporte a empresas</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center res_xs_mb_20">
                    <i class="ion-ios-locked animation" data-animation="fadeInUp" data-animation-delay="0.8s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="0.9s">41</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="1s">Amenazas mitigadas</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="box_counter text-center">
                    <i class="ion-android-contacts animation" data-animation="fadeInUp" data-animation-delay="1.1s"></i>
                    <h3 class="counter animation" data-animation="fadeInUp" data-animation-delay="1.2s">80</h3>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="1.3s">Desarrollos
                        implementados</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION COUNTER -->


<!-- START SECTION TOKEN SALE -->

<!-- END SECTION TOKEN SALE -->

<!-- SECTION WHITEPAPER -->

<!-- END SECTION WHITEPAPER -->
<!-- START SECTION TEAM -->
<section id="roadmap">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 offset-lg-2">
                <div class="title_blue_dark text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Proyectos</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s" align="justify">
                        Esquitechs tiene experiencia en distintos proyectos de ingeniería y ha desarrollado casos de
                        éxito en el sector gubernamental y en el sector financiero. Todos nuestros desarrollos están
                        adaptados específicamente a las características de sus clientes y son diseñados a medida.</p>
                </div>
            </div>
        </div>
        <div class="row small_space">
            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp"
                     data-animation-delay="0.4s">
                    <div class="text-center">
                        <img class="lazyload" data-src="assets/images/facturador.webp" alt="Sistema de facturación Operadora central de estacionamientos">
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team1" class="content-popup">Facturación Central</a></h4>
                        <p>APLICACIÓN WEB</p>
                    </div>
                    <div id="team1" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100 lazyload" data-src="assets/images/centralcompany.webp" alt="Operadora central de estacionamientos">
                                    <div class="team_title">
                                        <span>Sistema de facturación operadora central de estacionamientos</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>Central de Estacionamientos</h5>
                                    <hr>
                                    <p>Sistema web creado para la empresa central operativo de estacionamientos para
                                        gestion , validación y Administracion de facturación.</p>
                                    <a href="https://facturacion.central-mx.com">CONOCER</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp"
                     data-animation-delay="0.6s">
                    <div class="text-center">
                        <img class="lazyload" data-src="assets/images/m2.webp" alt="team2">
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team2" class="content-popup">M2</a></h4>
                        <p>ECOMMERCE</p>
                    </div>
                    <div id="team2" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100 lazyload" data-src="assets/images/m2.webp" alt="Ecommerce">
                                    <div class="team_title">
                                        <h4>M2</h4>
                                        <span>ECOMMERCE</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">

                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>M2</h5>
                                    <hr>
                                    <p>Cooperación en la creación de la pagina para compra, renta y venta de
                                        inmuebles.</p>
                                    <a href="https://m2.com.mx" class="btn btn-border-black btn-radius">CONOCELA <i
                                                class="ion-ios-arrow-thin-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp"
                     data-animation-delay="0.8s">
                    <div class="text-center">
                        <img class="lazyload" data-src="assets/images/credi.webp" alt="team3">
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team3" class="content-popup">CrediDigital</a></h4>
                        <p>APLICACIÓN WEB</p>
                    </div>
                    <div id="team3" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100 lazyload" data-src="assets/images/credi.webp" alt="Fovisste">
                                    <div class="team_title">
                                        <h4>CrediDigital</h4>
                                        <span>APLICACIÓN WEB</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>CrediDigital</h5>
                                    <hr>
                                    <p>"Cooperacion en la creacion" ,CrediDigital es una herramienta desde la cual
                                        podrás llevar a cabo la formalización de tu crédito de manera digital. Elige una
                                        Entidad Financiera autorizada por FOVISSSTE, sube tus documentos, entérate de
                                        fechas límite y más. Todo esto desde la comodidad de tu casa u oficina, a través
                                        de cualquier dispositivo móvil inteligente o una computadora..</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp"
                     data-animation-delay="1s">
                    <div class="text-center">
                        <img class="lazyload" data-src="assets/images/evento.webp" alt="Diseño">
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team4" class="content-popup">Black Thunder Team</a></h4>
                        <p>PAGINA WEB</p>
                    </div>
                    <div id="team4" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100 lazyload" data-src="assets/images/evento.webp" alt="Parkour team">
                                    <div class="team_title">
                                        <h4>>Black Thunder Team</h4>
                                        <span>PAGINA WEB</span>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>Black Thunder Team</h5>
                                    <hr>
                                    <p>Pagina de presentación con un videojuego del equipo de parkour "Black thunder
                                        team" (Actualmente en construccion).</p>
                                    <a href="https://blackthunder.esquitechs.com"
                                       class="btn btn-border-black btn-radius">CONOCELA <i
                                                class="ion-ios-arrow-thin-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider large_divider"></div>
    </div>
</section>


<section id="team" class="blue_bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-md-12 col-sm-12">
                <div class="title_default_light text_md_center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s" align="center">
                        Metodologias</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s" align="justify">En
                        esquitechs nos apegamos a la utilizacion de las metodologías de desarrollo de software, estas
                        metodologias de trabajo nos permiten estructurar, planear y controlar el proceso de desarrollo
                        en sistemas de información para que nuestros clientes esten monitoreando sus desarrollos en todo
                        momento.</p>
                </div>

            </div>
            <div class="col-lg-5 col-md-12 col-sm-12">
                <div class="res_md_mt_50 res_sm_mt_20 text-center animation" data-animation="fadeInRight"
                     data-animation-delay="0.2s">
                    <img class="lazyload" data-src="assets/images/banner_img1.webp" alt="mobile_app4">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- START SECTION TIMELINE -->
<section id="token">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-8 col-md-12 offset-lg-2">
                <div class="title_blue_dark text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Servicios</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s" align="center">Ofrecemos
                        una amplia variedad de servicios para hacer crecer tu negocio o empresa, automatizando procesos,
                        reduciendo costos y calidad de servicio.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row roadmap_list small_space align-items-end" align="justify">
            <div class="col-lg">
                <div class="single_roadmap roadmap_done">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Dominios</h6>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.3s">Compra ahora el nombre de
                        tu pagina web.</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap roadmap_done">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Marketing</h6>
                    <p class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Promociona tu web site
                        en nuestros sitios alojados y llega a más clientes.</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">ERP</h6>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Software diseñado para
                        usarse en cualquier sector.</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Software a la
                        medida</h6>
                    <p class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Nos especializamos
                        creando desarrollos que a la medida, satisfaciendo las necesidades de nuestros clientes.</p>
                </div>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Cuentas de correo</h6>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Correo profesional con tu
                        propio dominio y Soporte 24/7, da una imagen profesional con tu correo corporativo con tu mismo
                        dominio.</p>
                </div>
                <hr>
            </div>
            <div class="col-lg">
                <div class="single_roadmap">
                    <div class="roadmap_icon"></div>
                    <h6 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Servidores</h6>
                    <p class="animation" data-animation="fadeInDown" data-animation-delay="0.2s">Servidores confiables,
                        garantizados, servicio al cliente 24/7, Ancho de banda ilimitado, Carga rápida de páginas te
                        ofrecemos una actividad 99.8% en línea. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION TIMELINE -->

<!-- SECTION MOBILE APP -->

<!-- END SECTION MOBILE APP -->
<section class="blue_dark_bg" id="blog" align="center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-9">
                <div class="action-content res_md_mb_20">
                    <h3 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s" align="">¿Ya conoces lo
                        que tenemos para ti?</h3>
                    <p class="m-0 animation" data-animation="fadeInUp" data-animation-delay="0.4s" align="justify">
                        Nuestros objetivo es cumplir las metas empresariales, conocer las últimas noticias tecnologías y
                        las tendencias.</p>
                </div>
            </div>
            <!--<div class="col-lg-3 text-lg-right">
                <a href="404.html" class="btn btn-info" data-animation="fadeInUp" data-animation-delay="0.45">Blog<i class="ion-ios-arrow-thin-right"></i></a>
            </div>-->
        </div>
    </div>
</section>

<!-- END SECTION TEAM -->
<!--
<section class="section_team" id="faq">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 offset-lg-2">
                <div class="title_default_dark title_border text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Nuestro equipo</h4>
                    <p class="animation" data-animation="fadeInUp" data-animation-delay="0.4s"> Somos un grupo de profesionales que logramos alcanzar día a día los objetivos de nuestra compañía especializándose y actualizándose para resolver necedades empresariales.</p>
                </div>
            </div>
        </div>

        <div class="divider small_divider d_md_none"></div>
        <div class="row small_space">
            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.4s">
                    <div class="text-center">
                        <img src="assets/images/jhon.jpg" alt="team8">
                        <div class="team_social_s2 list_none">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team8" class="content-popup">Jonathan Vargas</a></h4>
                        <p>Full stack Developer</p>
                    </div>
                    <div id="team8" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/jhon.jpg" alt="user_img-lg">
                                    <div class="team_title">
                                        <h4>Jonathan Vargas</h4>
                                        <span>Full stack Developer</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>Spbre mi</h5>
                                    <hr>
                                    <p>Experto en desarrollo -HTML	  -CSS	-Javascript	       -Jquery       -PHP        -Bootstrap                      -Material Design   -Laravel    -Golang
                                        -Apis 	-Microservicios      -Linux</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 res_md_mb_30 res_sm_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.6s">
                    <div class="text-center">
                        <img src="assets/images/victor.png" alt="team9">
                        <div class="team_social_s2 list_none">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team9" class="content-popup">Victor Rojas</a></h4>
                        <p>Full stack Developer</p>
                    </div>
                    <div id="team9" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/victor.png" alt="user_img-lg">
                                    <div class="team_title">
                                        <h4>Victor Rojas</h4>
                                        <span>Full stack Developer</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>Sobre mi</h5>
                                    <hr>
                                    <p> Experto en desarrollo:-HTML	-Javascript          -VueJs
                                        -Bootstrap     -PHP       -Material Design   -Laravel 	 -Python      -.Net    -Java -Apis 	-Microservicios      -Linux</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 res_xs_mb_20">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="0.8s">
                    <div class="text-center">
                        <img src="assets/images/leo.png" alt="team10">
                        <div class="team_social_s2 list_none">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team10" class="content-popup">Leonardo Hernández</a></h4>
                        <p>Frontend Developer</p>
                    </div>
                    <div id="team10" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/leo.png" alt="user_img-lg">
                                    <div class="team_title">
                                        <h4>Leonardo Hernández</h4>
                                        <span>Frontend Developer</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>Sobre mi</h5>
                                    <hr>
                                    <p>Experto en -HTML	-CSS	-Javascript	       -Typescript     -AngularJs      -Bootstrap       -Material Design     	-C#</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="bg_gray4 radius_box team_box_s3 animation" data-animation="fadeInUp" data-animation-delay="1s">
                    <div class="text-center">
                        <img src="assets/images/omar.jpg" alt="team11">
                        <div class="team_social_s2 list_none">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="team_info text-center">
                        <h4><a href="#team11" class="content-popup">Omar Pérez</a></h4>
                        <p>Data Base Admin
                        </p>
                    </div>
                    <div id="team11" class="team_pop mfp-hide">
                        <div class="row m-0">
                            <div class="col-md-4 text-center">
                                <div class="team_img_wrap">
                                    <img class="w-100" src="assets/images/omar.jpg" alt="user_img-lg">
                                    <div class="team_title">
                                        <h4>Omar Pérez</h4>
                                        <span>Data Base Admin</span>
                                    </div>
                                </div>
                                <div class="social_single_team list_none mt-3">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="pt-3">
                                    <h5>Sobre mi</h5>
                                    <hr>
                                    <p>Experto en bases de datos -Oracle	-Mysql	-PL/SQL 	 -JAVA -C++ 	-Administración financiera</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider large_divider"></div>


        <div class="divider small_divider d_md_none"></div>


    </div>
</section>
-->
<!-- START SECTION FAQ -->

<!-- END SECTION FAQ -->

<!-- START CLIENTS SECTION -->
<section class="client_logo white_dark_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title_blue_dark text-center">
                    <h4 class="animation" data-animation="fadeInUp" data-animation-delay="0.2s">Nuestros Clientes</h4>
                </div>
            </div>
        </div>
        <div class="row align-items-center text-center overflow_hide small_space">
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation"
                     data-animation="fadeInUp" data-animation-delay="0.3s">
                    <img class="lazyload" data-src="assets/images/foviaste.webp" alt="fovisste">
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation"
                     data-animation="fadeInUp" data-animation-delay="0.4s">
                    <img class="lazyload" data-src="assets/images/centralcompany.webp" alt="Operadora central de estacionamientos">
                </div>
            </div>


            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation"
                     data-animation="fadeInUp" data-animation-delay="0.8s">
                    <H1><font color="#b7b7b7">M2</font></H1>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-6 logo_border">
                <div class="d-flex flex-wrap align-items-center justify-content-center h-100 animation"
                     data-animation="fadeInUp" data-animation-delay="0.8s">
                    <H1><font color="#b7b7b7">INTEGRADOR</font></H1>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CLIENTS SECTION -->

<!-- START SECTION CONTACT -->
<section id="contact">
    <div class="container wow fadeInUp">
        <div class="section-header">
            <h3 class="section-title" align="center">Contacto</h3>
            <p class="section-description"></p>
        </div>
    </div>

    <!-- Uncomment below if you wan to use dynamic maps -->
    <iframe class="lazyload" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30109.89733903326!2d-99.17905125661346!3d19.38053004649527!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ffa4134e58db%3A0xb31cef65890a450a!2sBenito%20Ju%C3%A1rez%2C%20Ciudad%20de%20M%C3%A9xico%2C%20CDMX!5e0!3m2!1ses!2smx!4v1567234972578!5m2!1ses!2smx"
            width="100%" height="380" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    <div class="container wow fadeInUp mt-5">
        <div class="row justify-content-center">

            <div class="col-lg-5 col-md-8">
                <div class="form">
                    <form action="correo.php" method="post" role="form">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Nombre"
                                   data-rule="minlen:4" data-msg="Porfavor ingrese minimo 4 letras"/>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                   data-rule="email" data-msg="Porfavor, ingrese un email valido"/>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto"
                                   data-rule="minlen:4" data-msg="Minimo 8 caracteres de sujeto"/>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required"
                                      data-msg="Porfavor, escriba algo" placeholder="Escriba su mensaje"></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="text-center">
                            <button type="submit">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
</section><!-- #contact -->
<!-- END SECTION CONTACT -->

<!-- START FOOTER SECTION -->
<footer>
    <div class="bottom_footer">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p class="copyright">Copyright &copy; 2019 Todos los derechos reservados.</p>

                </div>
                <div class="col-md-2">
                    <img class="logo_light logo_esquitechs lazyload" data-src="assets/images/Imagen66.webp" id="logo_esquitechs"
                         style="width: 60px;" alt="Empresa desarrollo de software">
                </div>
                <div class="col-md-5">
                    <ul class="list_none footer_menu">
                        <p class="copyright">Esquitechs 2019</p>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER SECTION -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="assets\css\style.min.css">
<!-- Favicon Icon -->
<link rel="shortcut icon" type="image/x-icon" href="assets/images/Imagen33.webp" alt="Desarrollo web">
<!-- Animation CSS -->
<link rel="stylesheet" href="assets\css\animate.css">
<!-- Latest Bootstrap min CSS -->
<link rel="stylesheet" href="assets\bootstrap\css\bootstrap.min.css">
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="assets\css\font-awesome.min.css">
<!-- ionicons CSS -->
<link rel="stylesheet" href="assets\css\ionicons.min.css">

<link rel="stylesheet" href="assets/css/chatcss.css">
<!--- owl carousel CSS-->
<link rel="stylesheet" href="assets\owlcarousel\css\owl.carousel.min.css">
<link rel="stylesheet" href="assets\owlcarousel\css\owl.theme.default.min.css">
<!-- Magnific Popup CSS -->
<link rel="stylesheet" href="assets\css\magnific-popup.css">
<link rel="stylesheet" href="assets\css\spop.min.css">
<!-- Style CSS -->
<link rel="stylesheet" href="assets\css\style.css">
<link rel="stylesheet" href="assets\css\responsive.css">
<!-- Color CSS -->
<link id="layoutstyle" rel="stylesheet" href="assets\color\theme.css">


<a href="#" class="scrollup btn-default"><i class="ion-ios-arrow-up"></i></a>
<script src="assets/js/lazysizes.min.js" async></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/chat.js"></script>
<script src="assets/js/interaccion.js"></script>
<!-- Latest jQuery -->
<script src="assets\js\jquery-1.12.4.min.js"></script>
<!-- Latest compiled and minified Bootstrap -->
<script src="assets\bootstrap\js\bootstrap.min.js"></script>
<!-- owl-carousel min js  -->
<script src="assets\owlcarousel\js\owl.carousel.min.js"></script>
<!-- magnific-popup min js  -->
<script src="assets\js\magnific-popup.min.js"></script>
<!-- waypoints min js  -->
<script src="assets\js\waypoints.min.js"></script>
<!-- parallax js  -->
<script src="assets\js\parallax.js"></script>
<!-- countdown js  -->
<script src="assets\js\jquery.countdown.min.js"></script>
<!-- particles min js  -->
<script src="assets\js\particles.min.js"></script>
<!-- scripts js -->
<script src="assets\js\jquery.dd.min.js"></script>
<!-- jquery.counterup.min js -->
<script src="assets\js\jquery.counterup.min.js"></script>
<!-- modern_canvas js -->
<script src="assets\js\modern_canvas.js"></script>
<!-- Google Map Js .-->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="assets\js\spop.min.js"></script>
<script src="assets\js\notification.js"></script>
<!-- scripts js -->
<script src="assets\js\scripts.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</body>
</html>